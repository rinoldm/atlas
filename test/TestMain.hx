import haxe.io.Output;

import atlas.TestBitSet;
import atlas.TestPropValue;
import atlas.TestRulesParser;

class TestMain {

  public static function main() {
    var tests = [];

    function addTest(name: String, fn: Void -> Void): Void {
      tests.push(new Test(name, fn));
    }

    addTest("BitSet: empty", TestBitSet.testEmpty);
    addTest("BitSet: singleton", TestBitSet.testSingleton);
    addTest("BitSet: from", TestBitSet.testFrom);
    addTest("BitSet: range", TestBitSet.testRange);
    addTest("BitSet: add and remove", TestBitSet.testAddAndRemove);
    addTest("BitSet: add and remove range", TestBitSet.testAddAndRemoveRange);
    addTest("BitSet: test a.and(b)", TestBitSet.testAnd);
    addTest("BitSet: test a.or(b)", TestBitSet.testOr);
    addTest("BitSet: test count", TestBitSet.testCount);
    addTest("BitSet: test min/max", TestBitSet.testMinMax);

    addTest("PropValue: basic parsing", TestPropValue.testParseDeclBasic);
    addTest("PropValue: invalid parsing", TestPropValue.testParseInvalid);
    addTest("PropValue: linear parsing", TestPropValue.testParseLinear);

    addTest("RuleParser: sample rules", TestRulesParser.testParseSampleRules);

    if (!Test.runTestSuite(tests, Sys.stdout())) {
      Sys.exit(1);
    }
  }
}
