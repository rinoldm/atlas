package atlas;

/**
  A property key for storing Atlas properties of type `V`.
**/
abstract Property<V>(Int) {
  private static var nextId: Int = getNextId();

  /**
    Creates a fresh property key distinct from all others.
  **/
  public inline function new() {
    this = getNextId();
  }

  private static inline function getNextId(): Int {
    // Workaround for static initialization order issues
    if (nextId == null) {
      nextId = 0;
    }
    return nextId++;
  }

  @:allow(atlas.ds.PropertyMap)
  private inline function asInt(): Int {
    return this;
  }

  /**
    Declare the name and type of an exported property.

    Convenience method for `PropBridge.new`.
  **/
  public inline function bridge(name: String, type: IPropType<V>): PropBridge {
    return new PropBridge(name, cast this, type);
  }
}
