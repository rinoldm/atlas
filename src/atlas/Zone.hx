package atlas;

import etwin.ds.Nil;
import atlas.ds.BitSet;

typedef ContiguousZone = {
  var did(default, null): Int;
  var start(default, null): Int;
  var end(default, null): Int;
};

/**
  A set of levels, as a list of `(did, lid)` pairs.
**/
class Zone {

  private var levels: Array<Null<BitSet>>;

  private var contiguousZone: Nil<Null<ContiguousZone>> = Nil.none();

  /**
    Creates an empty `Zone`.
  **/
  public function new(): Void {
    this.levels = [];
  }

  private inline function modifyWorld(did: Int): BitSet {
    this.contiguousZone = Nil.none();
    var b = this.levels[did];
    if (b == null) {
      b = BitSet.empty();
      this.levels[did] = b;
    }
    return b;
  }

  /**
    Adds the levels `[start, end)` of `did` to `this` zone.
  **/
  public function addRange(did: Int, start: Int, end: Int): Void {
    modifyWorld(did).addRange(start, end);
  }

  /**
    Adds the level `lid` of `did` to `this` zone.
  **/
  public function addLevel(did: Int, lid: Int): Void {
    modifyWorld(did).add(lid);
  }
  
  /**
    Adds all the levels of `other` to `this` zone.
  **/
  public function addAll(other: Zone): Void {
    this.contiguousZone = Nil.none();
    other.forEachWorld(function(did, set) {
      var cur = this.levels[did];
      if (cur == null) {
        this.levels[did] = set.copy();
      } else {
        cur.addAll(set);
      }
    });
  }

  private function computeContiguousZone(): Null<ContiguousZone> {
    var l = this.levels.length;
    var did = null;
    var world = null;
    for (d in 0...this.levels.length) {
      var w = this.levels[d];
      // Ignore empty sets.
      if (w != null && !w.isEmpty()) {
        // A zone in multiple worlds cannot be contiguous.
        if (world != null)
          return null;
        world = w;
        did = d;
      }
    }

    // Empty zones aren't contiguous.
    if (did == null) return null;

    var start = world.minValue();
    var end = world.maxValue() + 1;
    // The set has holes; it isn't contiguous.
    if (world.count() < end - start)
      return null;

    return { did: did, start: start, end: end };
  }

  /**
    If this zone is a single contiguous span of levels, returns this span.
  **/
  public function asContiguousZone(): Null<ContiguousZone> {
    this.contiguousZone.map(z => return z);
    var z = computeContiguousZone();
    this.contiguousZone = Nil.some(z);
    return z;
  }

  /**
    Calls the given function for each world of `this` zone containing levels.
  **/
  public inline function forEachWorld(f: Int -> BitSet -> Void): Void {
    for (did in 0...this.levels.length) {
      var set = this.levels[did];
      // Ignore empty sets.
      if (set != null && !set.isEmpty()) {
        f(did, set);
      }
    }
  }

  /**
    Returns a copy of `this` zone.
  **/
  public function copy(): Zone {
    var copy = new Zone();
    forEachWorld(function(did, set) {
      copy.levels[did] = set.copy();
    });
    return copy;
  }

  /**
    Returns a user-friendly representation of this `Zone`.
  **/
  public function toString(): String {
    var buf = new StringBuf();
    var sep = "{";

    forEachWorld(function(did, set) {
      buf.add(sep);
      buf.add(did);
      buf.add(" => ");
      buf.add(set.toString());
      sep = ", ";
    });
    
    if (buf.length == 0)
      return "{}";

    buf.add("}");
    return buf.toString();
  }
}
