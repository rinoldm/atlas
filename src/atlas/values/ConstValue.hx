package atlas.values;

import hf.mode.GameMode;

/**
  A constant property value.
**/
class ConstValue<V> implements IPropValue<V> {

  private var value(default, null): V;

  public inline function new(value: V) {
    this.value = value;
  }

  public inline function getAt(lid: Int): V {
    return value;
  }
}
