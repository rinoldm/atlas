package atlas.values;

import hf.mode.GameMode;
import patchman.Assert;

enum LinearValueType<V> {
  TInt(): LinearValueType<Int>;
  TFloat(): LinearValueType<Float>;
}

/**
  A numeric (`Int` or `Float`) property value that varies linearly.
**/
class LinearValue<V: Float> implements IPropValue<V> {

  // Level id of the initial value.
  private var startLid: Int;
  // The initial value.
  private var start: V;
  // How much to increment the value at each step.
  private var incr: Float;
  // Number of levels between each increment.
  private var step: Float;
  // Truncate the output value to an integer?
  private var needsTrunc: Bool;

  public function new(startLid: Int, start: V, incr: Float, step: Int, type: LinearValueType<V>) {
    Assert.debug(type != null);
    Assert.debug(step > 0);
    this.startLid = startLid;
    this.start = start;
    this.incr = incr;
    this.step = step;
    this.needsTrunc = switch (type) {
      case TInt: Std.int(incr) != incr;
      case TFloat: false;
    };
  }

  public function getAt(lid: Int): V {
    var steps = Math.floor((lid - this.startLid) / this.step);
    var v: Float = this.start + steps * this.incr;
    return cast (this.needsTrunc ? Std.int(v) : v);
  }
}
