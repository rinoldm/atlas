package atlas;

import atlas.ds.PropertyMap;
import atlas.ds.Erased;

/**
  An Atlas property parsable from the configuration.
**/
class PropBridge {

  private var name: String;
  @:allow(atlas.RulesContext)
  private var key: Property<Erased>;
  @:allow(atlas.RulesContext)
  private var type: IPropType<Erased>;

  public function new<V>(name: String, key: Property<V>, type: IPropType<V>) {
    this.name = name;
    this.key = cast key;
    this.type = cast type;
  }

  public inline function rename(name: String): PropBridge {
    return new PropBridge(name, this.key, this.type);
  }

  public inline function getName(): String {
    return this.name;
  }
}
