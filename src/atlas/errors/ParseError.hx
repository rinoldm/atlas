package atlas.errors;

/**
  An error occuring when parsing Atlas configuration data.
**/
class ParseError extends etwin.Error {

  private var isExpected: Bool;
  public var actual(default, null): Dynamic;

  private function new(actual: Dynamic, msg: String, isExpected: Bool): Void {
    super(msg);
    this.actual = actual;
    this.isExpected = isExpected;
    this.name = etwin.Obfu.raw("ParseError");
  }

  public override function toString(): String {
    if (this.isExpected) {
      return '${this.name}: ${this.message}, got: ${this.actual}';
    } else {
      return '${this.name}: ${this.message} (for: ${this.actual})';
    }
  }

  public static inline function expected(actual: Dynamic, expected: String): ParseError {
    return new ParseError(actual, "Expected " + expected, true);
  }

  public static inline function custom(actual: Dynamic, msg: String): ParseError {
    return new ParseError(actual, msg, false);
  }
}
