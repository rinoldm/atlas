package atlas.props;

import etwin.Obfu;
import patchman.IPatch;
import patchman.Ref;

/**
  Provides the `immortal: Bool` property, preventing the player from
  going under 0 lives.
**/
@:build(patchman.Build.di())
class Immortal {

    public static var KEY(default, null): Property<Bool> = new Property();

    public static var PATCH(default, null): IPatch = Ref.auto(hf.entity.Player.killPlayer)
    .before(function(hf, self) {
        var immortal: Bool = Atlas.getProperty(self.game, KEY).or(_ => false);
        if (immortal) {
            if (self.lives <= 0) {
                self.lives = 1;
            }
        }
    });

    @:diExport
    public var property(default, null): PropBridge = KEY.bridge(Obfu.raw("immortal"), PropTypes.BOOL);

    @:diExport
    public var patch(default, null): IPatch = PATCH;

    public function new() {}
}
