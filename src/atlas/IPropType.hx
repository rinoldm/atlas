package atlas;

/**
  Defines the type of an Atlas property by
  providing various conversion methods.

  See `PropTypes` for common types to use.
**/
interface IPropType<V> {

  /**
    Parse a `raw` value from a rule in the configuration file.
  **/
  function parseRuleValue(ctx: RulesContext, raw: Dynamic): IPropValue<V>;
}
