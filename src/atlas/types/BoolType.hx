package atlas.types;

import atlas.errors.ParseError;

/**
  A boolean property. See `PropTypes.BOOL` for details.
**/
class BoolType implements IPropType<Bool> {

  public function new() {}

  public function parseRuleValue(ctx: RulesContext, raw: Dynamic): IPropValue<Bool> {
    if (Std.is(raw, Bool)) {
      return raw ? PropValues.TRUE : PropValues.FALSE;
    }
    throw ParseError.expected(raw, "a boolean");
  }
}
