package atlas.types;

import etwin.Obfu;
import atlas.errors.ParseError;
import atlas.values.LinearValue;

/**
  A numeric property. See `PropTypes.INT` and `PropTypes.FLOAT` for details.
**/
class NumberType<T: Float> implements IPropType<T> {
  
  private var type: LinearValueType<T>;
  
  public function new(type: LinearValueType<T>) {
    this.type = type;
  }

  public function parseRuleValue(ctx: RulesContext, raw: Dynamic): IPropValue<T> {
    if (!Reflect.isObject(raw)) {
      return PropValues.of(parseNumber(raw));
    }

    var start = parseNumber(Obfu.field(raw, "start"));

    var zone = ctx.zone.asContiguousZone();
    if (zone == null) {
      throw ParseError.custom(raw, "Cannot use linear values in non-contiguous zones");
    }

    var end = Obfu.field(raw, "end");
    if (end != null) {
      return PropValues.lerp(zone.start, start, zone.end, parseNumber(end), this.type);
    }

    var incr = parseNumber(Obfu.field(raw, "incr"), 1);
    var step = parseInt(Obfu.field(raw, "step"), 1);
    if (step < 1) {
      throw ParseError.expected(step, "a positive integer");
    }

    return PropValues.stepping(zone.start, start, incr, step, this.type);
  }

  private function parseNumber(raw: Dynamic, ?defaultVal: Int): T {
    return switch (this.type) {
      case TInt: parseInt(raw, defaultVal);
      case TFloat: parseFloat(raw, defaultVal);
    };
  }

  private inline function parseFloat(raw: Dynamic, ?defaultVal: Float): Float {
    if (Std.is(raw, Float)) {
      return raw;
    } else if (defaultVal != null && raw == null) {
      return defaultVal;
    } else {
      throw ParseError.expected(raw, "a number");
    }
  }

  private inline function parseInt(raw: Dynamic, ?defaultVal: Int): Int {
    if (Std.is(raw, Int)) {
      return raw;
    } else if (defaultVal != null && raw == null) {
      return defaultVal;
    } else {
      throw ParseError.expected(raw, "an integer");
    }
  }
}
