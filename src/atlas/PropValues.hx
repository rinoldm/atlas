package atlas;

import atlas.values.ConstValue;
import atlas.values.LinearValue;

/**
  Factories for creating Atlas property values.
**/
class PropValues<V> {

  /**
    The constant `true`.
  **/
  public static var TRUE(default, never): IPropValue<Bool> = new ConstValue(true);

  /**
    The constant `false`.
  **/
  public static var FALSE(default, never): IPropValue<Bool> = new ConstValue(false);

  /**
    A constant value.
  **/
  public static inline function of<V>(val: V): IPropValue<V> {
    return new ConstValue(val);
  }

  /**
    A number varying linearly between `startVal` (at `startLid`) and `endVal` (at `endLid`).
  **/
  public static inline function lerp<V: Float>(startLid: Int, startVal: V, endLid: Int, endVal: V, type: LinearValueType<V>): IPropValue<V> {
    return new LinearValue(startLid, startVal, (endVal - startVal) / (endLid - startLid), 1, type);
  }

  /**
    A number beginning at `startVal` (at `startLid`), and increasing by `incr` every `step` levels. 
  **/
  public static inline function stepping<V: Float>(startLid: Int, startVal: V, incr: V, step: Int = 1, type: LinearValueType<V>): IPropValue<V> {
    return new LinearValue(startLid, startVal, incr, step, type);
  }
}
