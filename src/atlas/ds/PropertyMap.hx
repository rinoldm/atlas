package atlas.ds;

private typedef RawPropertyMap = Map<Int, IPropValue<Erased>>;

/**
  A map for storing Atlas properties of arbitrary type.
**/
abstract PropertyMap(RawPropertyMap) {

  public inline function new() {
    this = new Map();
  }

  private inline function raw(): RawPropertyMap {
    return this;
  }

  private static inline function fromRaw(raw: RawPropertyMap): PropertyMap {
    return cast raw;
  }

  // TODO: allow nulls?
  public inline function set<V>(key: Property<V>, value: IPropValue<V>): Void {
    this.set(key.asInt(), cast value);
  }

  public inline function get<V>(key: Property<V>): Null<IPropValue<V>> {
    return cast this.get(key.asInt());
  }

  public inline function copy(): PropertyMap {
    return fromRaw([for(k in this.keys()) k => this[k]]);
  }

  public inline function addAll(other: PropertyMap): Void {
    for (k in other.raw().keys()) {
      this.set(k, other.raw().get(k));
    }
  }
}
