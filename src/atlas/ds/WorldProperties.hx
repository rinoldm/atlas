package atlas.ds;

import etwin.ds.FrozenArray;
import etwin.ds.Nil;

/**
  Immutable storage for Atlas properties indexed by level id.
**/
// TODO: test this
class WorldProperties {

  private static var EMPTY_PROPS: PropertyMap = new PropertyMap();

  private var rules: FrozenArray<{ zone: BitSet, props: PropertyMap }>;

  private var defaultProps: PropertyMap;
  private var propsByZoneKey: Map<String, PropertyMap>;
  private var propsByLid: Array<PropertyMap>;

  /**
    Creates a new storage with the given rules.

    If several rules define the same property on a given `lid`, the latest value wins.
  **/
  public function new(
    rules: FrozenArray<{ zone: BitSet, props: PropertyMap }>,
    ?defaultProps: PropertyMap
  ) {
    this.rules = rules;
    this.defaultProps = defaultProps == null ? EMPTY_PROPS : defaultProps;
    this.propsByZoneKey = new Map();
    this.propsByLid = [];
  }

  private inline function makePropsAt(lid: Int): PropertyMap {
    var props = null;
    for (rule in this.rules) {
      if (!rule.zone[lid]) continue;

      if (props == null) {
        props = this.defaultProps.copy();
      }
      props.addAll(rule.props);
    }
    return props == null ? this.defaultProps : props;
  }

  private inline function makeZoneKey(lid: Int): String {
    var k = "";
    for (rule in this.rules) {
      k += rule.zone[lid] ? "1" : "0";
    }
    return k;
  }

  private inline function getPropsAt(lid: Int): PropertyMap {
    var props = this.propsByLid[lid];

    // This level isn't visited yet, look if it
    // belongs to a zone subset we've already seen. 
    if (props == null) {
      var key = makeZoneKey(lid);
      props = this.propsByZoneKey[key];

      // This level is in a new zone subset, compute its properties.
      if (props == null) {
        props = makePropsAt(lid);
        this.propsByZoneKey[key] = props;
      }

      this.propsByLid[lid] = props;
    }

    return props;
  }

  /**
    Returns the value of a property at a given `lid`, if it is defined here.
  **/
  public function getAt<V>(lid: Int, property: Property<V>): Nil<V> {
    var value = getPropsAt(lid).get(property);
    return value == null ? Nil.none() : Nil.some(value.getAt(lid));
  }
}
