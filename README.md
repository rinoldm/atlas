# @patchman/atlas

Define level zones and attach properties to them.

**Usage:**
```as
import hf.Hf;
import patchman.IPatch;
import patchman.Patchman;
import atlas.Atlas;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    // List of properties to enable.
    bossLevels: atlas.props.Boss,
    
    atlas: atlas.Atlas,
    patches: Array<IPatch>,
    hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
```

## Available properties

- `atlas.props.Boss`: provides the `boss` property.
- `atlas.props.Darkness`: provides the `darkness` property.
- `atlas.props.Ninjutsu`: provides the `ninjutsu` property.
- `atlas.props.NoFireball`: provides the `noFireball` property.
- `atlas.props.DisableTileCache`: provides the `disableTileCache` property.
- `atlas.props.Immortal`: provides the `immortal` property.
- `atlas.props.NoReward`: provides the `noReward` property.

See the documentation of these types for more details.

## `Atlas.json` example

```js
{
  // Define named zones that can be reused later.
  "zones": {
    "foo": { "start": "main+10", "end": "main+50" }
  },

  // Attach properties to zones.
  "rules": [{
    "zone": "myDimension", // The zone to attach properties to.
    "ninjutsu": 5,
    "darkness": { "start": 10, "end": 50 }
  }, {
    "zone": "foo", // Zones defined earlier can be reused.
    "darkness": 100
  }]
}
```

## `Atlas.json` format

The configuration is an object containing the following fields:

- `ignoredTags: Array<String>` (optional)  
  List of level tags to ignore when computing level chunks.

- `zones: Map<String, ZoneDefinition>` (optional)  
  Named zone aliases that can be referenced in later rules, or between each other.

- `rules: Array<{ zone: ZoneDefinition, props... }>` (optional)  
  List of rules, associating one or several properties to a `zone`. The names of the properties are
  matched with the `atlas.PropBridge`s exported by your mods; unknown names are ignored.

  See the documentation of `atlas.PropTypes` for a list of common value formats.

- `defaults: { props... }` (optional)  
  Default values that are used if no other rules apply; properties use the same format as `rules`.

`ZoneDefinition`s can be specified in several different ways:

- `String`
  - `"foo"` - A level chunk, or a named zone defined earlier.
  - `"foo+5"` - A single level relative to a level chunk.
- `{ did: Int }`  
  The entirety of a world.
- `{ did: Int, lid: Int }`  
  A single level, by absolute position.
- `{ did: Int, lid: Array<Int> }`  
  A list of levels, by absolute positions.
- `{ did: Int, start: Int, end: Int }`  
  A range of levels, by absolute positions (`end` is included in the range).
- `{ start: String, end: String }`  
  A range of levels, by level references; invalid if `start` and `end` reference levels in different worlds. Each endpoint can be:
  - `"foo"` - The name of a level chunk, referencing the first (for `start`) or last (for `end`) level of the chunk;
  - `"foo+5"` - A reference to a specific level (the `end` level is included in the range).
- `Array<ZoneDefinition>`  
  An union of sub-definitions.
